import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginPage from './containers/LoginPage';
import RegistrationPage from './containers/RegistrationPage';
import ResetPasswordPage from './containers/ResetPasswordPage';
import RecoverPasswordPage from './containers/RecoverPasswordPage';
import RecoverPasswordSuccessPage from './containers/RecoverPasswordSuccessPage';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/sign-in" element={<LoginPage />} /> 
        <Route path="/sign-up" element={<RegistrationPage />} /> 
        <Route path="/reset-password" element={<ResetPasswordPage />} />
        <Route path="/recover-password" element={<RecoverPasswordPage />} />
        <Route path="/recover-password-success" element={<RecoverPasswordSuccessPage />}/> 
        {/* the last two Routes will be change to the conditional render on one path="/recover-password" */}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
