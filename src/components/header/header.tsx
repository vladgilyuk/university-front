import React from 'react'
import styles from "./header.module.scss"

interface HeaderProps {
    heading: string;
    text?: string;
}

function Header({heading, text}: HeaderProps) {
    const headerClassName = `${text && text.length < 100 ? styles.centerText : styles.text}`;
    console.log(text)
    console.log(text && text.length )
  return (
    <>
    <img src="svgs/header-image.svg" alt="header" className={styles.centredImage}/>
    <h2 className={styles.heading}>{heading}</h2>
    {text && <p className={headerClassName}>{text}</p>}
    </>
  )
}

export default Header