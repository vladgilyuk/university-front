import React from 'react'
import styles from './checkbox.module.scss'; 
interface ChekboxProps {
    label: string;
    text: string;
    onChange?: () => void;
}
function Checkbox({label, text, onChange }:ChekboxProps) {
  return (
    
    <div className={styles['checkbox-container']}> 
    <input className={styles['checkbox-container__input']}  type="checkbox" name={label}  onChange={onChange}/>
    <label  className={styles['checkbox-container__label']} htmlFor={label}>{text}</label>
    </div>
  )
}

export default Checkbox