import React from 'react'
import styles from "./input.module.scss"

interface LoginProps {
  label: string;
  type: string;
}


function Input({label, type}:LoginProps) {
  return (
    <div className={styles['input-container']}>
      <label className={styles['input-container__label']} htmlFor={label}>{label}</label>
      <input className={styles['input-container__input']} type={type} id={label} name={label}/>
    </div>
  )
}

export default Input