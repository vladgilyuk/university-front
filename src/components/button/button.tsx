import React from 'react'
import styles from "./button.module.scss"
interface ButtonProps {
    text: string;
}

function Button({text} : ButtonProps) {
    const buttonClassName = `${text === 'Cancel' ? styles.cancelButton : styles.defaultButton}`;
  return (
     <button className={buttonClassName}>{text}</button>
  )
}

export default Button