import {useState} from 'react'
import Header from '../components/header/header';
import Input from '../components/input/input';
import Checkbox from '../components/checkbox/checkbox';
import Button from '../components/button/button';
import styles from "../styles/commonStyles.module.scss";

function LoginPage() {
  const [showPasswords, setShowPasswords] = useState(false);

  const handleCheckboxChange = () => {
    setShowPasswords(!showPasswords);
  };
  return (
    <>
    <form className={styles.form}>
    <Header heading="Welcome!" />
    <Input label="Email" type="email"/>
    <Input label="Password" type={showPasswords ? 'text' : 'password'}/>
    <Checkbox label="Password" text="Show Password" onChange={handleCheckboxChange} />
    <Button  text='Login'/>
    </form>
    </>
  )
}

export default LoginPage