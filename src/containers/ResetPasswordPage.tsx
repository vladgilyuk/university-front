import React from 'react'
import Header from '../components/header/header';
import Input from '../components/input/input';
import Button from '../components/button/button';
import styles from "../styles/commonStyles.module.scss";

function ResetPasswordPage() {
  return (
    <>
    <form className={styles.form}>
    <Header heading="Reset Password" text="Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset." />
    <Input label="Email" type="email" />
    <div className={styles.buttonContainer}>
        <Button text='Reset' />
        <Button text='Cancel' />
    </div>
    </form>
    </>
  )
}

export default ResetPasswordPage