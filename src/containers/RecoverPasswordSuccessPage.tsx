import React from 'react'
import Header from '../components/header/header';
import Button from '../components/button/button';
import styles from "../styles/commonStyles.module.scss";

function RecoverPasswordSuccessPage() {
  return (
    <>
    <form className={styles.form}>
    <Header heading="Password Changed" text="You can use your new password to log into your account" />
    <Button text='Log In' />
    </form>
    </>
  )
}

export default RecoverPasswordSuccessPage