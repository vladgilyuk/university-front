import {useState} from 'react'
import Header from '../components/header/header';
import Input from '../components/input/input';
import Checkbox from '../components/checkbox/checkbox';
import Button from '../components/button/button';
import styles from "../styles/commonStyles.module.scss";

function RecoverPasswordPage() {
  const [showPasswords, setShowPasswords] = useState(false);

  const handleCheckboxChange = () => {
    setShowPasswords(!showPasswords);
  };
  return (
    <>
    <form className={styles.form}>
    <Header heading={"Reset your password!"} />
    <Input label="New Password" type={showPasswords ? 'text' : 'password'}/>
    <Input label="Confirm Password" type={showPasswords ? 'text' : 'password'}/>
    <Checkbox label="Password" text="Show Password" onChange={handleCheckboxChange}/>
    <Button text='Reset'/>
    </form>
    </>
  )
}

export default RecoverPasswordPage