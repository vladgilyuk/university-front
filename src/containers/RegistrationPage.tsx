import {useState} from 'react'
import Header from '../components/header/header';
import Input from '../components/input/input';
import Checkbox from '../components/checkbox/checkbox';
import Button from '../components/button/button';
import styles from "../styles/commonStyles.module.scss";

function RegistrationPage() {
  const [showPasswords, setShowPasswords] = useState(false);

  const handleCheckboxChange = () => {
    setShowPasswords(!showPasswords);
  };
  return (
    <>
    <form className={styles.form}>
    <Header heading={"Register your account"}  />
    <Input label="Email" type="email"/>
    <Input label="Password" type={showPasswords ? 'text' : 'password'}/>
    <Input label="Confirm Password" type={showPasswords ? 'text' : 'password'}/>
    <Checkbox label="Password" text="Show Password" onChange={handleCheckboxChange}/>
    <Button text='Register'/>
    </form>
    </>
  )
}

export default RegistrationPage